from django.test import TestCase, LiveServerTestCase
from django.http import HttpRequest
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

from .views import loginUser, registerUser, logoutUser

# Create your tests here.
class UnitTest(TestCase):

    def test_url_does_exist(self):
        response = self.client.get('/tugas9/')
        self.assertEqual(response.status_code, 200)
        response2 = self.client.get('/tugas9/logout/')
        self.assertEqual(response2.status_code, 302)
        response3 = self.client.get('/tugas9/register/')
        self.assertEqual(response3.status_code, 200)
    
    def test_using_correct_template(self):
        response = self.client.get('/tugas9/')
        self.assertTemplateUsed(response, 'loginApp/login.html')
        response3 = self.client.get('/tugas9/register/')
        self.assertTemplateUsed(response3, 'loginApp/register.html')
    
    def test_url_fires_correct_method(self):
        response = resolve('/tugas9/')
        self.assertEqual(response.func, loginUser)
        response2 = resolve('/tugas9/logout/')
        self.assertEqual(response2.func, logoutUser)
        response3 = resolve('/tugas9/register/')
        self.assertEqual(response3.func, registerUser)
    
    def test_text_inside_html(self):
        request = HttpRequest()
        response = loginUser(request)
        html_response = response.content.decode('utf8')
        self.assertIn("SELAMAT",html_response)
    
    def test_trying_login(self):
        response = self.client.post('/tugas9/', data={'user_name' : 'coba', 'Password' : 'coba'})
        html_response = response.content.decode()
        self.assertIn(html_response, 'Halo, coba')
    
    def test_trying_register_with_existed_account(self):
        response = self.client.post('/tugas9/register/', data={'user_name' : 'coba', 'Password' : 'coba'})
        html_response = response.content.decode()
        self.assertIn(html_response, 'SELAMAT')

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
    
    def test_main(self):
        self.selenium.get('http://localhost:8000/tugas9/')
        self.selenium.find_element_by_name('user_name').send_keys('coba')
        self.selenium.find_element_by_name('Password').send_keys('coba')
        self.selenium.find_element_by_name('button-login').click()
        time.sleep(5)
        self.assertIn('Halo', self.selenium.page_source)
        self.assertIn('BERHASIL LOGIN', self.selenium.page_source)
        self.selenium.find_element_by_name('button-logout').click()
        time.sleep(5)
        self.assertIn('Password', self.selenium.page_source)