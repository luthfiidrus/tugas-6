from django.test import TestCase, LiveServerTestCase
from django.urls import resolve
from .views import bukuViews
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class UnitTest(TestCase):

    def test_url_is_exist(self):
        response = self.client.get('/tugas8/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_fires_correct_views_method(self):
        reseponse = resolve('/tugas8/')
        self.assertEqual(reseponse.func, bukuViews)
    
    def test_views_render_correct_html(self):
        response = self.client.get('/tugas8/')
        self.assertTemplateUsed(response, 'buku/buku.html')

    def test_json_data(self):
        response = self.client.get('/tugas8/jsondata/')
        self.assertEqual(response.status_code, 200)

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
    
    def test_functional(self):
        self.selenium.get("http://localhost:8000/tugas8/")
        self.assertIn("Story 8", self.selenium.page_source)
        self.selenium.find_element_by_name("search").send_keys("a")
        time.sleep(5)
        self.assertIn("A", self.selenium.page_source)