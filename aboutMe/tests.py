from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from .views import accordion
from django.test import LiveServerTestCase
from selenium import webdriver
import unittest
from selenium.webdriver.chrome.options import Options

# # Create your tests here.

class UnitTesting(TestCase):

    def test_page(self):
        response = self.client.get("/tugas7/")
        self.assertEqual(response.status_code,200)
    
    def test_page_template(self):
        response = self.client.get("/tugas7/")
        self.assertTemplateUsed(response,'aboutMe/aboutme.html')
    
    def test_greetings(self):
        request = HttpRequest()
        response = accordion(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Halo! Saya Muhammad Luthfi Idrus",html_response)

class FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()

    def test_mode(self):
        self.selenium.get("http://localhost:8000/tugas7")
        self.assertIn("Berikut adalah deskripsi kecil mengenai saya.", self.selenium.page_source)
        self.selenium.find_element_by_name('tema1').click()
        bg = self.selenium.find_element_by_name('body').value_of_css_property('background-color')
        self.assertIn('rgba(255, 255, 255, 1)', bg)
        self.selenium.find_element_by_name('tema2').click()
        bg = self.selenium.find_element_by_name('body').value_of_css_property('background-color')
        self.assertIn('rgba(0, 0, 0, 1)', bg)
        self.assertIn('<div class="titik" id="tema1" name="tema1"', self.selenium.page_source)
        self.assertIn('<div class="titik" id="tema2" name="tema2"', self.selenium.page_source)
        self.selenium.find_element_by_name('toggle1').click()
        self.assertIn("Belajar dan fokus kepada akademis", self.selenium.page_source)
        self.selenium.find_element_by_name('toggle2').click()
        self.assertIn("DPM Fasilkom UI", self.selenium.page_source)
        self.selenium.find_element_by_name('toggle3').click()
        self.assertIn("Belum ada", self.selenium.page_source)
        

