from django.shortcuts import render, redirect
from .forms import landingPageForms
from .models import Status

# Create your views here.
def landingPageViews(request) :
    if request.method == 'POST' :
        form = landingPageForms(request.POST)
        if (form.is_valid()) :
            db = Status()
            db.status = form.cleaned_data['status_anda']
            db.save()
        return redirect('/')
    else :
        form = landingPageForms()
        db = Status.objects.all()
        response = {
            'form' : form,
            'status' : db
        }
    return render(request, 'statusApp/landingpage.html', response)