from django import forms

class landingPageForms(forms.Form) :
    status_anda = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Silahkan isi status anda',
        'type' : 'text',
        'required' : True,
        'maxlength' : 300
    }))