from django.test import TestCase, LiveServerTestCase
from django.urls import resolve
from django.utils import timezone
from datetime import datetime
from .views import landingPageViews
from .models import Status
from .forms import landingPageForms
from selenium import webdriver
import time

# Create your tests here.
class TestingStatusApp(TestCase) :

    def test_does_url_exist(self) :
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_does_url_fires_the_correct_views_function(self) :
        response = resolve('/')
        self.assertEqual(response.func, landingPageViews)
    
    def test_does_views_render_the_correct_templates(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'statusApp/landingpage.html')
    
    def test_can_we_create_object(self) :
        response = self.client.post('/',data={'status_anda' : 'sedang belajar'})
        self.assertEqual(Status.objects.count(), 1)

    def test_can_we_create_object_without_fill_anything(self) :
        form = landingPageForms(data={'status_anda' : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['status_anda'], ['This field is required.'])
    
    def test_does_objects_that_we_create_shows_up(self) :
        response = self.client.post('/', data={'status_anda' : 'sedang belajar'})
        html_response = response.content.decode()
        self.assertIn(html_response, 'sedang belajar')

class functionalTestOnStatusApp(LiveServerTestCase) :

    def setUp(self) :
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='./chromedriver')
    
    def tearDown(self) :
        self.driver.quit()
        super().tearDown()
    
    def test_functional(self) :
        self.driver.get('http://localhost:8000/')
        self.driver.find_element_by_name('status_anda').send_keys('sedang belajar')
        self.driver.find_element_by_name('inputan').click()
        html_response = self.driver.page_source
        self.assertIn('sedang belajar', html_response)
