from django.db import models

# Create your models here.
class Status(models.Model) :

    status = models.CharField(max_length=300, blank=False)
    date = models.DateTimeField(auto_now_add=True, null=True) 