from django.urls import path
from .views import landingPageViews

urlpatterns = [
    path('', landingPageViews),
]